# luckymoney

#### 介绍
[廖师兄的第一门springboot课程](https://coding.imooc.com/class/328.html)<br/>
主要内容：

- 第一个SpringBoot程序
- 自定义属性配置
- Controller的使用
- spring-data-jpa
- 事务管理
