package com.liujx.luckymoney.controller;

import com.liujx.luckymoney.util.LimitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @Value("${height}")
    private Integer height;

    @Autowired
    private LimitConfig limitConfig;

    @RequestMapping(value = "/say1", method = RequestMethod.GET)
    public String say1() {
        return "学习SpringBoot，使用RequestMapping";
    }

    @GetMapping("/say2")
    public String say2() {
        return "学习SpringBoot，使用GetMapping";
    }

    @GetMapping("say3")
    public String say3() {
        return "学习@Value注解，height: "+height;
    }

    @GetMapping("getLuckymoneyConfig")
    private String getLuckymoneyConfig() {
        return "说明：" + limitConfig.getDescription();
    }

    @PostMapping("/getId")
    private String getId(@RequestParam(value = "id", required = false, defaultValue = "0") Integer myId) {
        return "id:" + myId;
    }
    
    @GetMapping("/say/{id}")
    public String say(@PathVariable("id") Integer id) {
        return "id:"+ id;
    }



}
