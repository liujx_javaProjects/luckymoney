package com.liujx.luckymoney.service;

import com.liujx.luckymoney.pojo.Luckymoney;
import com.liujx.luckymoney.repository.LuckymoneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
public class LuckymoneyService {

    @Autowired
    private LuckymoneyRepository repository;

    /**
     * 事务 指数据库事务
     * 如：扣库存 > 创建订单
     */
    @Transactional
    public void createTwo() {
        Luckymoney luckymoney1 = new Luckymoney();
        luckymoney1.setProducer("刘佳星");
        luckymoney1.setMoney(new BigDecimal("123"));
        repository.save(luckymoney1);

        Luckymoney luckymoney2 = new Luckymoney();
        luckymoney2.setProducer("刘星星");
        luckymoney2.setMoney(new BigDecimal("1234"));
        repository.save(luckymoney2);
    }
}
