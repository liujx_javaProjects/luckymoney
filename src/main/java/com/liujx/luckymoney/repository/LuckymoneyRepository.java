package com.liujx.luckymoney.repository;

import com.liujx.luckymoney.pojo.Luckymoney;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LuckymoneyRepository extends JpaRepository<Luckymoney, Integer> {
}
